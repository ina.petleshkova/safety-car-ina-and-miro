package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.City;
import com.telerikacademy.safetycar.repositories.CityRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class CityServiceTests {

    @InjectMocks
    CityServiceImpl mockCityService;

    @Mock
    CityRepository mockCityRepository;

    @Test
    public void getAll_ShouldReturn_EmptyListOfCities() {
        // Arrange
        Mockito.when(mockCityRepository.getAll())
                .thenReturn(Collections.emptyList());

        // Act
        List<City> cities = mockCityService.getAll();

        // Assert
        Assertions.assertEquals(0, cities.size());
    }

    @Test
    public void getAll_ShouldReturn_ListOfCities() {
        // Arrange
        List<City> list = new ArrayList<>();
        City city = new City();
        list.add(city);
        Mockito.when(mockCityRepository.getAll())
                .thenReturn(list);

        // Act
        List<City> cities = mockCityService.getAll();

        // Assert
        Assertions.assertEquals(1, cities.size());
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        City originalCity = new City();
        Mockito.when(mockCityRepository.getById(1))
                .thenReturn(originalCity);

        // Act
        City returnedCity = mockCityService.getById(1);

        // Assert
        Assertions.assertEquals(returnedCity, originalCity);
    }

}