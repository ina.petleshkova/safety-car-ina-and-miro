package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Make;
import com.telerikacademy.safetycar.repositories.MakeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class MakeServiceTests {

    @InjectMocks
    MakeServiceImpl mockMakeService;

    @Mock
    MakeRepository mockMakeRepository;

    @Test
    public void getAll_ShouldReturn_EmptyListOfMakes() {
        // Arrange
        Mockito.when(mockMakeRepository.getAll())
                .thenReturn(Collections.emptyList());

        // Act
        List<Make> makes = mockMakeService.getAll();

        // Assert
        Assertions.assertEquals(0, makes.size());
    }

    @Test
    public void getAll_ShouldReturn_ListOfMakes() {
        // Arrange
        List<Make> makesList = new ArrayList<>();
        Make make = new Make();
        makesList.add(make);
        Mockito.when(mockMakeRepository.getAll())
                .thenReturn(makesList);

        // Act
        List<Make> makes = mockMakeService.getAll();

        // Assert
        Assertions.assertEquals(1, makes.size());
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        Make originalMake = new Make();
        Mockito.when(mockMakeRepository.getById(Mockito.anyInt()))
                .thenReturn(originalMake);

        // Act
        Make returnedMake = mockMakeService.getById(Mockito.anyInt());

        // Assert
        Assertions.assertEquals(returnedMake, originalMake);
    }
}
