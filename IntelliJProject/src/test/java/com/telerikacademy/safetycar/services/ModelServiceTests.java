package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Make;
import com.telerikacademy.safetycar.models.Model;
import com.telerikacademy.safetycar.repositories.ModelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ModelServiceTests {

    @InjectMocks
    ModelServiceImpl mockModelService;

    @Mock
    ModelRepository mockModelRepository;

    @Test
    public void getAll_ShouldReturn_EmptyListOfModels() {
        // Arrange
        Mockito.when(mockModelRepository.getAll())
                .thenReturn(Collections.emptyList());

        // Act
        List<Model> models = mockModelService.getAll();

        // Assert
        Assertions.assertEquals(0, models.size());
    }

    @Test
    public void getAll_ShouldReturn_ListOfMakes() {
        // Arrange
        List<Model> modelsList = new ArrayList<>();
        Model model = new Model();
        modelsList.add(model);
        Mockito.when(mockModelRepository.getAll())
                .thenReturn(modelsList);

        // Act
        List<Model> models = mockModelService.getAll();

        // Assert
        Assertions.assertEquals(1, models.size());
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        Model originalModel = new Model();
        Mockito.when(mockModelRepository.getById(Mockito.anyInt()))
                .thenReturn(originalModel);

        // Act
        Model returnedModel = mockModelService.getById(Mockito.anyInt());

        // Assert
        Assertions.assertEquals(returnedModel, originalModel);
    }

    @Test
    public void getByMakeId_ShouldReturn_EmptyListOfModels_WhenNoModelsForThisMake() {
        // Arrange
        Mockito.when(mockModelRepository.getByMakeId(Mockito.anyInt()))
                .thenReturn(Collections.emptyList());

        // Act
        List<Model> models = mockModelService.getByMakeId(Mockito.anyInt());

        // Assert
        Assertions.assertEquals(0, models.size());
    }


    @Test
    public void getByMakeId_ShouldReturn_ListOfMakes() {
        // Arrange
        List<Model> modelsList = new ArrayList<>();
        Make make = new Make();
        make.setId(1);
        Model model = new Model();
        model.setMake(make);
        modelsList.add(model);
        Mockito.when(mockModelRepository.getByMakeId(Mockito.anyInt()))
                .thenReturn(modelsList);

        // Act
        List<Model> models = mockModelService.getByMakeId(1);

        // Assert
        Assertions.assertEquals(1, models.size());
    }

}
