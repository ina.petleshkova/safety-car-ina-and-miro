package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Status;
import com.telerikacademy.safetycar.repositories.StatusRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class StatusServiceTests {

    @InjectMocks
    StatusServiceImpl statusService;

    @Mock
    StatusRepository mockStatusRepository;

    @Test
    public void getAll_ShouldReturn_EmptyListOfStatuses_WhenNoStatuses() {
        // Arrange
        Mockito.when(mockStatusRepository.getAll())
                .thenReturn(Collections.emptyList());

        // Act
        List<Status> statusList = statusService.getAll();

        // Assert
        Assertions.assertEquals(0, statusList.size());
    }

    @Test
    public void getAll_ShouldReturn_ListOfStatuses_WhenThereAreAny() {
        // Arrange
        List<Status> list = new ArrayList<>();
        Status status = new Status();
        list.add(status);
        Mockito.when(mockStatusRepository.getAll())
                .thenReturn(list);

        // Act
        List<Status> statusList = statusService.getAll();

        // Assert
        Assertions.assertEquals(1, statusList.size());
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        Status originalStatus = new Status();
        Mockito.when(mockStatusRepository.getById(Mockito.anyInt()))
                .thenReturn(originalStatus);

        // Act
        Status returnedStatus = statusService.getById(Mockito.anyInt());

        // Assert
        Assertions.assertEquals(returnedStatus, originalStatus);
    }

    @Test
    public void getByStatusName_ShouldReturn_WhenValidId() {
        // Arrange
        Status originalStatus = new Status();
        Mockito.when(mockStatusRepository.getByStatus(Mockito.anyString()))
                .thenReturn(originalStatus);

        // Act
        Status returnedStatus = statusService.getByStatusName(Mockito.anyString());

        // Assert
        Assertions.assertEquals(returnedStatus, originalStatus);
    }
}
