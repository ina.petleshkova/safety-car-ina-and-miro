package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.VerificationToken;
import com.telerikacademy.safetycar.repositories.VerificationTokenRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class VerificationTokenServiceTests {

    @InjectMocks
    VerificationTokenServiceImpl mockVerificationTokenService;

    @Mock
    VerificationTokenRepository mockVerificationTokenRepository;

    @Test
    public void findByToken_ShouldReturn_WhenTokenExists() {
        // Arrange
        VerificationToken searchedToken = new VerificationToken();
        Mockito.when(mockVerificationTokenRepository.findByToken(Mockito.anyString()))
                .thenReturn(searchedToken);

        // Act
        VerificationToken returnedToken = mockVerificationTokenService.findByToken(Mockito.anyString());

        // Assert
        Assertions.assertEquals(returnedToken, searchedToken);
    }

    @Test
    public void findByUser_ShouldReturn_WhenUserExists() {
        // Arrange
        VerificationToken verificationToken = new VerificationToken();
        User user = new User();
        Mockito.when(mockVerificationTokenRepository.findByUser(user))
                .thenReturn(verificationToken);

        // Act
        VerificationToken returnedToken = mockVerificationTokenService.findByUser(user);

        // Assert
        Assertions.assertEquals(returnedToken, verificationToken);
    }

    @Test
    public void save_ShouldReturn_WhenExpirySet() {
        // Arrange
        User user = new User();
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setToken(Mockito.anyString());
        String token = verificationToken.getToken();

        // Act
        mockVerificationTokenService.save(user, token);

        // Assert
        Mockito.verify(mockVerificationTokenRepository, Mockito.times(1))
                .save(Mockito.any());
    }

}
