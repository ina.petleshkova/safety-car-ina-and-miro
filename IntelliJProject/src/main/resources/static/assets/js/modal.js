function openModal(requestNumber) {
    var modal = document.getElementById("myModal-" + requestNumber);
    modal.style.display = "block";
    var span = document.getElementsByClassName("close-" + requestNumber)[0];

    span.onclick = function () {
        modal.style.display = "none";
    }
}
