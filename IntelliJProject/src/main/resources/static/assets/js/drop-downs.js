/**
 * Code that loads asynchronously the drop-downs for car makes and then car models (from the selected car make)
 */


$(document).ready(function () {
    loadMakes();

    $("select#makes-dropdown").change(function () {
        console.log("this", $(this));
        var id = $(this).children("option:selected").val();
        console.log("id", id);
        clearModels();
        loadModels(id);
    });

    $("select#models-dropdown").change(function () {
        var title = $(this).children("option:selected").text();
        $('#models').append('<p>' + title + '</p>');
    });
});

function loadMakes() {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/api/makes',
        success: function (data) {
            console.log("makes");
            console.log(data);
            $('#makes-dropdown').append('<option selected disabled th:value="-1">Select the make of your car</option>')
            $('#models-dropdown').append('<option selected disabled th:value="-1">After selecting the make, select the model</option>')
            $.each(data, function (key, value) {
                var html = createOptionHtml(value.id, value.name);
                // console.log("value", value);
                console.log($('#makes-dropdown'));
                $('#makes-dropdown').append(html);
            });
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function loadModels(makeId) {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/api/models/make/' + makeId,
        success: function (data) {
            console.log(data);
            $('#models-dropdown').append('<option selected disabled th:value="-1">Select the model</option>')
            $.each(data, function (key, value) {
                var html = createOptionHtml(value.id, value.model);
                // console.log(html);
                console.log($('#makes-dropdown'));
                $('#models-dropdown').append(html);
            });
        }
    });
}

function clearModels() {
    $('#models-dropdown').empty();
}

function createOptionHtml(key, value) {
    return '<option value="' + key + '">' + value + '</option>';
}