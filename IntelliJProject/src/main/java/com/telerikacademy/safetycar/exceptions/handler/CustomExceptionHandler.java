package com.telerikacademy.safetycar.exceptions.handler;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;

import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(basePackages = "com.telerikacademy.safetycar.controllers.mvc")
public class CustomExceptionHandler {
    private final Log LOGGER = LogFactory.getLog(CustomExceptionHandler.class);

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ModelAndView handleEntityNotFound(EntityNotFoundException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/errors/404");
        mav.addObject("message", e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = {UnauthorizedOperationException.class})
    public ModelAndView handleEntityNotFound(UnauthorizedOperationException e) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("/access-denied");
        mav.addObject("message", e.getMessage());
        LOGGER.warn(e);
        return mav;
    }

}
