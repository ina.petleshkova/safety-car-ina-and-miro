package com.telerikacademy.safetycar.utils;

import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.dtos.*;
import com.telerikacademy.safetycar.services.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class SimulationOfferMapper {
    private final SimulationOfferService simulationOfferService;
    private final ModelService modelService;

    public SimulationOfferMapper(SimulationOfferService simulationOfferService, ModelService modelService) {
        this.simulationOfferService = simulationOfferService;
        this.modelService = modelService;
    }

    public SimulationOffer getSimulationOfferFromDTO(SimulationOfferDTO simulationOfferDTO) {
        SimulationOffer simulationOffer = new SimulationOffer();
        simulationOffer.setCubicCapacity(simulationOfferDTO.getCubicCapacity());
        simulationOffer.setFirstRegistrationDate(simulationOfferDTO.getFirstRegistrationDate());
        simulationOffer.setDriverAge(simulationOfferDTO.getDriverAge());
        simulationOffer.setPrevYearAccidents(simulationOfferDTO.getPrevYearAccidents());
        double premium = simulationOfferService.calculatePremium(simulationOffer);
        premium = roundToTwoDecimalDigits(premium);
        simulationOffer.setPremium(premium);
        simulationOfferDTO.setPremium(premium);
        Model model = modelService.getById(simulationOfferDTO.getModelId());
        simulationOffer.setModel(model);
        return simulationOffer;
    }

    private double roundToTwoDecimalDigits(double value) {
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
