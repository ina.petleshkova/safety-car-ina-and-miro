package com.telerikacademy.safetycar.utils;

import java.util.Base64;

public class ConvertImage {

    public String getConvertedImage(byte[] byteImage) {
        return Base64.getMimeEncoder().encodeToString(byteImage);
    }

}
