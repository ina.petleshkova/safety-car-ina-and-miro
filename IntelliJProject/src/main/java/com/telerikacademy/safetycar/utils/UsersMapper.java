package com.telerikacademy.safetycar.utils;

import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.models.dtos.ShowUsersDTO;
import com.telerikacademy.safetycar.models.dtos.UserRegistrationDTO;
import com.telerikacademy.safetycar.services.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UsersMapper {
    private final PasswordEncoder passwordEncoder;
    private final UserInfoService userInfoService;

    @Autowired
    public UsersMapper(PasswordEncoder passwordEncoder, UserInfoService userInfoService) {
        this.passwordEncoder = passwordEncoder;
        this.userInfoService = userInfoService;
    }

    public UserInfo userInfoFromDto(UserRegistrationDTO dto) {
        UserInfo userInfo = new UserInfo();
        userInfo.setEmail(dto.getEmail());
        userInfo.setFirstName(dto.getFirstName());
        userInfo.setLastName(dto.getLastName());
        userInfo.setPhone(dto.getPhone());
        userInfoService.create(userInfo);
        userInfo.setDeleted(true);
        userInfoService.update(userInfo);
        return userInfo;
    }

    public ShowUsersDTO getShowUsersDto(UserInfo userInfo) {
        ShowUsersDTO dto = new ShowUsersDTO();
        dto.setFirstName(userInfo.getFirstName());
        dto.setLastName(userInfo.getLastName());
        dto.setEmail(userInfo.getEmail());
        dto.setPhone(userInfo.getPhone());
        return dto;
    }

    public List<ShowUsersDTO> getShowAllUsers(List<UserInfo> userInfos) {
        return userInfos.stream().map(this::getShowUsersDto).collect(Collectors.toList());
    }

    public org.springframework.security.core.userdetails.User securityUserFromDto(UserRegistrationDTO dto) {

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        String encodedPassword = passwordEncoder.encode(dto.getPassword());

        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        dto.getEmail(), encodedPassword, false, true, true, true, authorities);
        return newUser;
    }

}
