package com.telerikacademy.safetycar.utils;


import com.telerikacademy.safetycar.models.ReCaptchaResponse;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static com.telerikacademy.safetycar.utils.Constants.RECAPTCHA_RESPONSE;
import static com.telerikacademy.safetycar.utils.Constants.RECAPTCHA_URL;

@Component
public class ReCaptchaHandler {

    private final RestTemplate restTemplate;

    public ReCaptchaHandler(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ReCaptchaResponse getReCaptchaResponse(String captchaResponse) {
        String params = RECAPTCHA_RESPONSE + captchaResponse;

        ReCaptchaResponse reCaptchaResponse = restTemplate.exchange(RECAPTCHA_URL + params, HttpMethod.POST, null, ReCaptchaResponse.class).getBody();
        return reCaptchaResponse;
    }

}
