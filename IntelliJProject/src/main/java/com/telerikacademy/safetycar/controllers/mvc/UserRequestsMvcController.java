package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.models.dtos.UpdatePolicyStatusDTO;
import com.telerikacademy.safetycar.services.PolicyRequestService;
import com.telerikacademy.safetycar.services.UserInfoService;
import com.telerikacademy.safetycar.services.UserService;
import com.telerikacademy.safetycar.utils.ConvertImage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/my-requests")
public class UserRequestsMvcController {

    private final PolicyRequestService policyRequestService;
    private final UserService userService;
    private final UserInfoService userInfoService;

    public UserRequestsMvcController(PolicyRequestService policyRequestService, UserService userService, UserInfoService userInfoService) {
        this.policyRequestService = policyRequestService;
        this.userService = userService;
        this.userInfoService = userInfoService;
    }

    @GetMapping
    public String showAllRequests(Model model, Principal principal) {
        UserInfo userInfo = userInfoService.getByEmail(principal.getName());
        model.addAttribute("requests", policyRequestService.getByCreator(userInfo.getId()));
        model.addAttribute("dto", new UpdatePolicyStatusDTO());
        model.addAttribute("convertImage", new ConvertImage());
        return "my-requests";
    }

    @PostMapping("/{id}")
    public String cancelPolicyRequest(@PathVariable int id,
                                            Principal principal) {
        User loggedUser = userService.getByEmail(principal.getName());
        PolicyRequest policyRequest = policyRequestService.getById(id);
        policyRequestService.cancel(policyRequest, loggedUser);
        return "redirect:/my-requests";
    }
}
