package com.telerikacademy.safetycar.controllers.rest;


import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Model;
import com.telerikacademy.safetycar.services.ModelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.telerikacademy.safetycar.utils.Constants.*;

@RestController
@RequestMapping("/api/models")
public class ModelRestController {

    private final ModelService modelService;

    @Autowired
    public ModelRestController(ModelService modelService) {
        this.modelService = modelService;
    }

    @GetMapping
    @ApiOperation(value = LIST_ALL_CAR_MODELS, response = List.class)
    public List<Model> getAll() {
        return modelService.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = GET_CAR_MODEL_BY_ID, response = Model.class)
    public Model getById(@PathVariable int id) {
        try {
            return modelService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/make/{makeId}")
    @ApiOperation(value = GET_CAR_MODELS_BY_MAKE_ID, response = List.class)
    public List<Model> getByMakeId(@PathVariable int makeId) {
        try {
            return modelService.getByMakeId(makeId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    
}
