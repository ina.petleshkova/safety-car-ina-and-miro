package com.telerikacademy.safetycar.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeMvcController {

    @GetMapping("/")
    public String getHomePage() {
        return "index";
    }

    @GetMapping("/access-denied")
    public String showAccessDeniedPage() {
        return "access-denied";
    }

    @GetMapping("/terms")
    public String getTermsPage() {
        return "terms";
    }

    @GetMapping("/about-us")
    public String getAboutUsPage() {
        return "about-us";
    }
}
