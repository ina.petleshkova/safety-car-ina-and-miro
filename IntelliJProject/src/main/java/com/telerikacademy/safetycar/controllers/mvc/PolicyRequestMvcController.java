package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.exceptions.InvalidInputException;
import com.telerikacademy.safetycar.models.*;
import com.telerikacademy.safetycar.models.dtos.CreatePolicyRequestDTO;
import com.telerikacademy.safetycar.services.*;
import com.telerikacademy.safetycar.utils.PolicyRequestModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;

@Controller
@RequestMapping("/policy")
public class PolicyRequestMvcController {
    private final PolicyRequestService policyRequestService;
    private final SimulationOfferService simulationOfferService;
    private final CityService cityService;
    private final UserInfoService userInfoService;
    private final PolicyRequestModelMapper policyRequestModelMapper;

    public PolicyRequestMvcController(PolicyRequestService policyRequestService,
                                      SimulationOfferService simulationOfferService,
                                      CityService cityService,
                                      UserInfoService userInfoService,
                                      PolicyRequestModelMapper policyRequestModelMapper) {
        this.policyRequestService = policyRequestService;
        this.simulationOfferService = simulationOfferService;
        this.cityService = cityService;
        this.userInfoService = userInfoService;
        this.policyRequestModelMapper = policyRequestModelMapper;
    }

    @GetMapping
    public String showCreatePolicyRequestPage(Model model, HttpServletRequest request) {
        model.addAttribute("createPolicyRequestDTO", new CreatePolicyRequestDTO());

        HttpSession session = request.getSession(false);
        if (session.getAttribute("offer") == null) {
//            request.getSession();
//            model.addAttribute("offer", new SimulationOffer());
//            return "create-offer-in-policy-request";
            return "create-policy-request";
        } else {
            SimulationOffer offer = (SimulationOffer) session.getAttribute("offer");
            model.addAttribute("offer", offer);
        }
        return "create-policy-request";
    }

    @PostMapping
    public String handleCreatePolicyRequest(@Valid @ModelAttribute(value = "createPolicyRequestDTO") CreatePolicyRequestDTO createPolicyRequestDTO,
                                            BindingResult bindingResult,
                                            Principal principal,
                                            Model model,
                                            HttpSession session) {
        try {
            SimulationOffer offer = (SimulationOffer) session.getAttribute("offer");
            model.addAttribute("offer", offer);

            if (bindingResult.hasErrors()) {
                model.addAttribute("createPolicyRequestDTO", createPolicyRequestDTO);
                return "create-policy-request";
            }

            UserInfo loggedUserInfo = userInfoService.getByEmail(principal.getName());
            PolicyRequest policyRequest = policyRequestModelMapper.getPolicyRequestFromDTO(createPolicyRequestDTO, loggedUserInfo);
            simulationOfferService.create(offer);
            policyRequest.setSimulationOffer(offer);
            policyRequestService.create(policyRequest);
            return "redirect:/policy/policy-confirmation";
        } catch (InvalidInputException e) {
            model.addAttribute("invalidStartDate", e.getMessage());
            return "create-policy-request";
        }
    }

    @GetMapping("/policy-confirmation")
    public String showPolicyConfirmation(HttpSession session) {
        session.removeAttribute("offer");
        return "policy-confirmation";
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("cities", cityService.getAll());
    }

}
