package com.telerikacademy.safetycar.controllers.rest;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.InvalidInputException;
import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.models.dtos.CreatePolicyRequestDTO;
import com.telerikacademy.safetycar.models.dtos.SimulationOfferDTO;
import com.telerikacademy.safetycar.services.*;
import com.telerikacademy.safetycar.utils.PolicyRequestModelMapper;
import com.telerikacademy.safetycar.utils.SimulationOfferMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static com.telerikacademy.safetycar.utils.Constants.*;
import static com.telerikacademy.safetycar.utils.LoggedUserValidator.checkIfUserIsLogged;

@RestController
@RequestMapping("/api/requests")
public class PolicyRequestRestController {

    private final PolicyRequestService policyRequestService;
    private final PolicyRequestModelMapper policyMapper;
    private final SimulationOfferMapper simulationOfferMapper;
    private final SimulationOfferService simulationOfferService;
    private final UserInfoService userInfoService;

    @Autowired
    public PolicyRequestRestController(PolicyRequestService policyRequestService,
                                       PolicyRequestModelMapper policyMapper,
                                       SimulationOfferMapper simulationOfferMapper,
                                       SimulationOfferService simulationOfferService,
                                       UserInfoService userInfoService) {
        this.policyRequestService = policyRequestService;
        this.policyMapper = policyMapper;
        this.simulationOfferMapper = simulationOfferMapper;
        this.simulationOfferService = simulationOfferService;
        this.userInfoService = userInfoService;
    }

    @GetMapping
    @ApiOperation(value = SHOW_ALL_POLICY_REQUESTS, response = List.class)
    public List<PolicyRequest> getAll(Principal principal) {
        checkIfUserIsLogged(principal);
        return policyRequestService.getAll();
    }

    @PostMapping("/new")
    @ApiOperation(value = CREATE_FULL_POLICY, response = PolicyRequest.class)
    public PolicyRequest create(@RequestPart("file") MultipartFile file,
                                @Valid @RequestPart("offer") SimulationOfferDTO simulationOfferDTO,
                                @Valid @RequestPart("policy") CreatePolicyRequestDTO createPolicyRequestDTO,
                                Principal principal) {
        try {
            checkIfUserIsLogged(principal);
            SimulationOffer offer = simulationOfferMapper.getSimulationOfferFromDTO(simulationOfferDTO);
            simulationOfferService.create(offer);
            createPolicyRequestDTO.setVehicleRegistrationImage(file);
            UserInfo loggedUserInfo = userInfoService.getByEmail(principal.getName());
            PolicyRequest policyRequest = policyMapper.getPolicyRequestFromDTO(createPolicyRequestDTO, loggedUserInfo);
            policyRequest.setSimulationOffer(offer);
            policyRequestService.create(policyRequest);
            return policyRequest;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidInputException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
