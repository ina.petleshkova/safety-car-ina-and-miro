package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.FilterRequestsDTO;
import com.telerikacademy.safetycar.models.dtos.UpdatePolicyStatusDTO;
import com.telerikacademy.safetycar.services.*;
import com.telerikacademy.safetycar.utils.ConvertImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/requests")
public class AgentMvcController {
    private final PolicyRequestService policyRequestService;
    private final UserService userService;
    private final EmailService emailService;
    private final StatusService statusService;

    @Autowired
    public AgentMvcController(PolicyRequestService policyRequestService, UserService userService, EmailService emailService, StatusService statusService) {
        this.policyRequestService = policyRequestService;
        this.userService = userService;
        this.emailService = emailService;
        this.statusService = statusService;
    }

    @GetMapping
    public String showAllRequests(Model model) {
        model.addAttribute("dto", new UpdatePolicyStatusDTO());
        model.addAttribute("filterRequestsDto", new FilterRequestsDTO());
        model.addAttribute("convertImage", new ConvertImage());
        model.addAttribute("statuses", statusService.getAll());
        return "requests-agents";
    }

    @PostMapping("/{id}")
    public String changeStatusPolicyRequest(@PathVariable int id, @ModelAttribute(value = "dto") UpdatePolicyStatusDTO dto,
                                            Principal principal,
                                            Model model) {
        model.addAttribute("dto", dto);
        User loggedUser = userService.getByEmail(principal.getName());
        PolicyRequest policyRequest = policyRequestService.getById(id);
        String status = dto.getStatus();
        policyRequestService.approveOrDeny(policyRequest, loggedUser, status);
        String email = policyRequest.getEmail();
        emailService.sendStatusChangeEmail(policyRequest, email);

        return "redirect:/requests";
    }

    @PostMapping()
    public String filterRequests(FilterRequestsDTO dto, Model model) {

        model.addAttribute("dto", new UpdatePolicyStatusDTO());
        model.addAttribute("statuses", statusService.getAll());
        model.addAttribute("filterRequestsDto", dto);
        model.addAttribute("convertImage", new ConvertImage());
        List<PolicyRequest> filter = policyRequestService.filter(dto.getStatus(), dto.getCreatorEmail(), dto.getHolderEmail(), dto.getRequestNumber(), dto.sortParam, dto.orderParam);
        model.addAttribute("requests", filter);
        return "requests-agents";
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("requests", policyRequestService.getAll());
    }

}
