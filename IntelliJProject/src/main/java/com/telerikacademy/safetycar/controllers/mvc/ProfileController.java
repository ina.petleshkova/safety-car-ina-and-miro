package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.services.UserService;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.ui.Model;
import com.telerikacademy.safetycar.models.dtos.UserPasswordChangeDTO;
import com.telerikacademy.safetycar.services.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.security.Principal;

import static com.telerikacademy.safetycar.utils.Constants.INVALID_CURRENT_PASSWORD;
import static com.telerikacademy.safetycar.utils.Constants.THE_PASSWORDS_DON_T_MATCH;

@Controller
@RequestMapping("/profile")
public class ProfileController {


    private final UserInfoService userInfoService;
    private final UserService userService;
    private final UserDetailsManager userDetailsManager;

    @Autowired
    public ProfileController(UserInfoService userInfoService, UserService userService, UserDetailsManager userDetailsManager) {
        this.userInfoService = userInfoService;
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
    }

    @GetMapping
    public String showProfilePage (Model model, Principal principal) {
        UserInfo loggedUserInfo = userInfoService.getByEmail(principal.getName());
        model.addAttribute("user", loggedUserInfo);
        return "profile-info";
    }

    @GetMapping("/pass")
    public String showChangePasswordPage (Model model) {
        model.addAttribute("userPasswordChangeDTO", new UserPasswordChangeDTO());
        return "password-change";
    }

    @PostMapping("/pass")
    public String handleChangePassword (Principal principal, @Valid @ModelAttribute UserPasswordChangeDTO userPasswordChangeDTO, BindingResult bindingResult, Model model) {
        model.addAttribute("userPasswordChangeDTO");

        if (bindingResult.hasErrors()) {
            return "password-change";
        }

        if (!userPasswordChangeDTO.getNewPassword().equals(userPasswordChangeDTO.getPasswordConfirmation())) {
            model.addAttribute("passMatch", THE_PASSWORDS_DON_T_MATCH);
            return "password-change";
        }

        org.springframework.security.core.userdetails.UserDetails loggedUser = userDetailsManager.loadUserByUsername(principal.getName());

        if (!userPasswordChangeDTO.getCurrentPassword().equals(loggedUser.getPassword())) {
            model.addAttribute("invalidPass", INVALID_CURRENT_PASSWORD);
            return "password-change";
        }

        org.springframework.security.core.userdetails.User newPassUser = userService.changeSecurityUserPass(userPasswordChangeDTO, loggedUser);

        userDetailsManager.updateUser(newPassUser);

        return "index";
    }

}
