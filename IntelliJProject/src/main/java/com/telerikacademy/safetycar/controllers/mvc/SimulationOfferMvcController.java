package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.exceptions.InvalidInputException;
import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.models.dtos.SimulationOfferDTO;
import com.telerikacademy.safetycar.services.*;
import com.telerikacademy.safetycar.utils.SimulationOfferMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


@Controller
@RequestMapping("/offer")
public class SimulationOfferMvcController {
    private final MakeService makeService;
    private final ModelService modelService;
    private final SimulationOfferMapper simulationOfferMapper;

    public SimulationOfferMvcController(MakeService makeService,
                                        ModelService modelService,
                                        SimulationOfferMapper simulationOfferMapper) {
        this.makeService = makeService;
        this.modelService = modelService;
        this.simulationOfferMapper = simulationOfferMapper;
    }

    @GetMapping
    public String showCreateOfferPage(Model model) {
        model.addAttribute("simulationOfferDTO", new SimulationOfferDTO());
        return "create-simulation-offer";
    }

    @PostMapping
    public String handleCreateOffer(@Valid @ModelAttribute(value = "simulationOfferDTO") SimulationOfferDTO simulationOfferDTO,
                                    BindingResult bindingResult,
                                    Model model,
                                    HttpServletRequest request) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("simulationOfferDTO", simulationOfferDTO);
            return "create-simulation-offer";
        }
        try {
            SimulationOffer simulationOffer = simulationOfferMapper.getSimulationOfferFromDTO(simulationOfferDTO);
            HttpSession session = request.getSession();
            session.setAttribute("offer", simulationOffer);
            return "create-simulation-offer";
        } catch (InvalidInputException e) {
            model.addAttribute("regDateError", e.getMessage());
            return "create-simulation-offer";
        }
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("makes", makeService.getAll());
        model.addAttribute("models", modelService.getAll());
    }

}
