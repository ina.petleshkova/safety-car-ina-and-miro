package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.ReCaptchaResponse;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.models.VerificationToken;
import com.telerikacademy.safetycar.models.dtos.UserRegistrationDTO;
import com.telerikacademy.safetycar.services.EmailService;
import com.telerikacademy.safetycar.services.UserInfoService;
import com.telerikacademy.safetycar.services.UserService;
import com.telerikacademy.safetycar.services.VerificationTokenService;
import com.telerikacademy.safetycar.utils.ReCaptchaHandler;
import com.telerikacademy.safetycar.utils.UsersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Optional;
import java.util.UUID;

import static com.telerikacademy.safetycar.utils.Constants.*;

@Controller
public class RegistrationMvcController {

    String message;

    private final UserDetailsManager userDetailsManager;
    private final UserInfoService userInfoService;
    private final UserService userService;
    private final VerificationTokenService verificationTokenService;
    private final EmailService emailService;
    private final ReCaptchaHandler reCaptchaHandler;
    private final UsersMapper usersMapper;

    @Autowired
    public RegistrationMvcController(UserDetailsManager userDetailsManager,
                                     UserInfoService userInfoService,
                                     UserService userService,
                                     VerificationTokenService verificationTokenService,
                                     EmailService emailService, ReCaptchaHandler reCaptchaHandler, UsersMapper usersMapper) {
        this.userDetailsManager = userDetailsManager;
        this.userInfoService = userInfoService;
        this.userService = userService;
        this.verificationTokenService = verificationTokenService;
        this.emailService = emailService;
        this.reCaptchaHandler = reCaptchaHandler;
        this.usersMapper = usersMapper;
    }

    @GetMapping("/registration-confirmation")
    public String showRegConfirm() {
        return "registration-confirmation";
    }

    @GetMapping("/register")
    public String showRegistrationPage(Model model) {
        model.addAttribute("user", new UserRegistrationDTO());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(
            @Valid @ModelAttribute(name = "user") UserRegistrationDTO dto,
            BindingResult bindingResult,
            @RequestParam(name = "g-recaptcha-response") String captchaResponse,
            Model model) {

        model.addAttribute("user", dto);

        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!dto.getPassword().equals(dto.getPasswordConfirmation())) {
            model.addAttribute("passMatch", THE_PASSWORDS_DON_T_MATCH);
            return "register";
        }

        if (userDetailsManager.userExists(dto.getEmail())) {
            model.addAttribute("mailTaken", USER_WITH_THIS_EMAIL_ALREADY_EXISTS);
            return "register";
        }

        //re-captcha handling
        ReCaptchaResponse reCaptchaResponse = reCaptchaHandler.getReCaptchaResponse(captchaResponse);
        if (!reCaptchaResponse.isSuccess()) {
            message = PLEASE_VERIFY_NOT_A_ROBOT;
            model.addAttribute("reCaptchaError", message);
            return "register";
        }

        // create User for security
        org.springframework.security.core.userdetails.User newUser = usersMapper.securityUserFromDto(dto);
        userDetailsManager.createUser(newUser);

        // create user in DB
        User user = userService.getByEmail(dto.getEmail());
        UserInfo userInfo = usersMapper.userInfoFromDto(dto);
        user.setUserInfo(userInfo);

        // check if user is created and if yes - save token in DB
        Optional<User> saved = Optional.of(user);
        try {
            String token = UUID.randomUUID().toString();
            verificationTokenService.save(saved.get(), token);
            emailService.sendRegistrationMail(user);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:/registration-confirmation";

    }

    @GetMapping("/activation")
    public String activation(@RequestParam("token") String token, Model model) {
        VerificationToken verificationToken = verificationTokenService.findByToken(token);
        User user = verificationToken.getUser();
        if (!user.isEnabled()) {
            Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
            if (verificationToken.getExpiry().before(currentTimestamp)) {
                model.addAttribute("message", VERIFICATION_TOKEN_EXPIRED_ERROR_MESSAGE);
            } else {
                user.setEnabled(true);
                userService.update(user, user);
                UserInfo userInfo = userInfoService.getByEmail(user.getUsername());
                userInfo.setDeleted(false);
                userInfoService.update(userInfo);
                model.addAttribute("message", ACCOUNT_ACTIVATED_MESSAGE);
            }
        }
        return "activation";
    }


}