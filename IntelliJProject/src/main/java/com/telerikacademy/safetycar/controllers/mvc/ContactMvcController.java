package com.telerikacademy.safetycar.controllers.mvc;

import com.telerikacademy.safetycar.models.dtos.ContactDTO;
import com.telerikacademy.safetycar.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/contact")
public class ContactMvcController {

    private final EmailService emailService;

    @Autowired
    public ContactMvcController(EmailService emailService) {
        this.emailService = emailService;
    }

    @GetMapping
    public String showContactForm(Model model) {
        model.addAttribute("contact", new ContactDTO());
        return "contact";
    }

    @PostMapping
    public String contact(@Valid @ModelAttribute(name = "contact") ContactDTO dto,
                          BindingResult bindingResult, Model model) {
        model.addAttribute("contact", dto);

        if (bindingResult.hasErrors()) {
            return "contact";
        }
        emailService.contactEmail(dto);
        return "contact-message-sent";
    }
}
