package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.City;

import java.util.List;

public interface CityRepository {

    List<City> getAll();

    City getById(int id);

}
