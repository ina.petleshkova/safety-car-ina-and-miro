package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.User;

public interface UserRepository {

    void update(User user);

    void delete(User user);

    User getByEmail(String email);

}
