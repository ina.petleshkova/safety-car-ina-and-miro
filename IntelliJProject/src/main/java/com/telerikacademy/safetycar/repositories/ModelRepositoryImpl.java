package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Model;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModelRepositoryImpl implements ModelRepository {
    SessionFactory sessionFactory;

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Model> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery("from Model", Model.class);
            return query.list();
        }
    }

    @Override
    public Model getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Model model = session.get(Model.class, id);
            if (model == null) {
                throw new EntityNotFoundException(
                        String.format("Car model with ID %d not found!", id));
            }
            return model;
        }
    }

    @Override
    public List<Model> getByMakeId(int makeId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery("from Model where make.id = ?1", Model.class);
            query.setParameter(1, makeId);
            return query.list();
        }
    }

}
