package com.telerikacademy.safetycar.repositories;


import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.exceptions.InvalidInputException;
import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.utils.DatesHelperMethods;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import static com.telerikacademy.safetycar.utils.Constants.*;

import java.util.Date;
import java.util.List;

@Repository
public class SimulationOfferRepositoryImpl implements SimulationOfferRepository {

    SessionFactory sessionFactory;

    @Autowired
    public SimulationOfferRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<SimulationOffer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<SimulationOffer> query = session.createQuery("from SimulationOffer", SimulationOffer.class);
            return query.list();
        }
    }

    @Override
    public SimulationOffer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            SimulationOffer simulationOffer = session.get(SimulationOffer.class, id);
            if (simulationOffer == null) {
                throw new EntityNotFoundException(
                        String.format("Offer with ID %d not found!", id));
            }
            return simulationOffer;
        }
    }

    @Override
    public void create(SimulationOffer simulationOffer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(simulationOffer);
        }
    }

    private Integer getCarAge(String registrationDate) {
        Date regDate = DatesHelperMethods.parseDate(registrationDate);
        int age =  DatesHelperMethods.getAge(regDate);
        if (age<0) {
            throw new InvalidInputException(CAR_REGISTRATION_DATE_ERROR);
        }
        return age;
    }

    public double getBaseAmount(SimulationOffer simulationOffer) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> query = session.createNativeQuery("select mc.base_amount from safety_car.multicriteria_calculator mc where ?1 >= mc.cc_min and ?1 <= cc_max and ?2 >= car_age_min and ?2 <= car_age_max");
            query.setParameter(1, simulationOffer.getCubicCapacity());
            query.setParameter(2, getCarAge(simulationOffer.getFirstRegistrationDate()));
            if (query.getResultList().isEmpty()) {
                throw new EntityNotFoundException(CALCULATE_PREMIUM_ERROR_MESSAGE);
            }
            return query.getResultList().get(0);
        }
    }

    @Override
    public void update(SimulationOffer simulationOffer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(simulationOffer);
            session.getTransaction().commit();
        }
    }

}
