package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Status;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatusRepositoryImpl implements StatusRepository{
    SessionFactory sessionFactory;

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Status> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery("from Status order by id", Status.class);
            return query.list();
        }
    }

    @Override
    public Status getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Status status = session.get(Status.class, id);
            if (status == null) {
                throw new EntityNotFoundException(
                        String.format("Status with ID %d not found!", id));
            }
            return status;
        }
    }

    @Override
    public Status getByStatus(String statusName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery("from Status where statusName = :statusName", Status.class);
            query.setParameter("statusName", statusName);
            List<Status> statuses  = query.list();
            Status status = statuses.get(0);
            if (status == null) {
                throw new EntityNotFoundException(
                        String.format("Status %s not found!", statusName));
            }
            return status;
        }
    }
}
