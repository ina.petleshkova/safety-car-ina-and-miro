package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.models.dtos.SimulationOfferDTO;

import java.util.List;

public interface SimulationOfferRepository {

    List<SimulationOffer> getAll();

    SimulationOffer getById(int id);

    void create(SimulationOffer simulationOffer);

    double getBaseAmount(SimulationOffer simulationOffer);

    void update(SimulationOffer simulationOffer);

}
