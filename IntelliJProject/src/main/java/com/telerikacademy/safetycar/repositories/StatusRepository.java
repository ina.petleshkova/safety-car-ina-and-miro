package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.Status;

import java.util.List;

public interface StatusRepository {

    List<Status> getAll();

    Status getById(int id);

    Status getByStatus(String StatusName);
}
