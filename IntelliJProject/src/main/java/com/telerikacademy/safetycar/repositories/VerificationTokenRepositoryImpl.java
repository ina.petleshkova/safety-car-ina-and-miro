package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.VerificationToken;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import static com.telerikacademy.safetycar.utils.Constants.*;

import java.util.List;

@Repository
public class VerificationTokenRepositoryImpl implements VerificationTokenRepository {


    SessionFactory sessionFactory;

    @Autowired
    public VerificationTokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public VerificationToken findByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<VerificationToken> query = session.createQuery("from VerificationToken where token = :token", VerificationToken.class);
            query.setParameter("token", token);
            List<VerificationToken> resultList = query.getResultList();
            if (resultList.isEmpty()) {
                throw new EntityNotFoundException(MISSING_TOKEN_ERROR_MESSAGE);
            }
            return resultList.get(0);
        }
    }

    @Override
    public VerificationToken findByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<VerificationToken> query = session.createQuery("from VerificationToken where user = :user", VerificationToken.class);
            query.setParameter("user", user);
            List<VerificationToken> resultList = query.getResultList();
            if (resultList.isEmpty()) {
                throw new EntityNotFoundException(MISSING_TOKEN_ERROR_MESSAGE);
            }
            return resultList.get(0);
        }
    }

    @Override
    public void save(VerificationToken verificationToken) {
        try (Session session = sessionFactory.openSession()) {
            session.save(verificationToken);
        }
    }

}
