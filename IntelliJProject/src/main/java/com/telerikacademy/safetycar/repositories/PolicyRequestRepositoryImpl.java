package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.UserInfo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class PolicyRequestRepositoryImpl implements PolicyRequestRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public PolicyRequestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PolicyRequest> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<PolicyRequest> query = session.createQuery("from PolicyRequest where status.id != 4 order by requestNumber desc", PolicyRequest.class);
            return query.list();
        }
    }

    @Override
    public PolicyRequest getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PolicyRequest policyRequest = session.get(PolicyRequest.class, id);
            if (policyRequest == null) {
                throw new EntityNotFoundException(
                        String.format("Policy with id %d not found!", id));
            }
            return policyRequest;
        }
    }

    @Override
    public List<PolicyRequest> getByCreator(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<PolicyRequest> query = session.createQuery("from PolicyRequest where owner.id = :owner order by requestNumber desc ", PolicyRequest.class);
            query.setParameter("owner", userId);
            return query.list();
        }
    }

    @Override
    public void create(PolicyRequest policyRequest) {
        try (Session session = sessionFactory.openSession()) {
            session.save(policyRequest);
        }
    }

    @Override
    public List<PolicyRequest> filter(Optional<Integer> status,
                                      Optional<String> creatorEmail,
                                      Optional<String> holderEmail,
                                      Optional<Integer> requestNumber,
                                      Optional<String> sortParam,
                                      String orderParam) {
        List<PolicyRequest> filteredPolicies = new ArrayList<>();
        if (status.isEmpty() && creatorEmail.isEmpty() &&
                holderEmail.isEmpty() && requestNumber.isEmpty()) {
            return new ArrayList<>();
        }
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<PolicyRequest> cq = cb.createQuery(PolicyRequest.class);
            Root<PolicyRequest> policyRoot = cq.from(PolicyRequest.class);
            List<Predicate> predicates = new ArrayList<>();

            if (status.isPresent() && status.get() != 0) {
                predicates.add(cb.equal(policyRoot.get("status"), status.get()));
            }

            if (creatorEmail.isPresent() && !creatorEmail.get().equals("")) {
                predicates.add(cb.like(policyRoot.get("owner").get("email"), "%" + creatorEmail.get() + "%"));
            }

            if (holderEmail.isPresent() && !holderEmail.get().equals("")) {
                predicates.add(cb.like(policyRoot.get("email"), "%" + holderEmail.get() + "%"));
            }

            if (requestNumber.isPresent() && requestNumber.get() != 0) {
                predicates.add(cb.equal(policyRoot.get("requestNumber"), requestNumber.get()));
            }

            predicates.add(cb.equal(policyRoot.get("deleted"), false));
            cq.select(policyRoot).distinct(true).where(predicates.toArray(Predicate[]::new));

            if (sortParam.isPresent()) {
                Path<PolicyRequest> policyPath;
                switch (sortParam.get()) {
                    case "Status":
                        policyPath = policyRoot.get("status");
                        getAscOrDesc(orderParam, cb, cq, policyPath);
                        break;
                    case "StartDate":
                        policyPath = policyRoot.get("startDate");
                        getAscOrDesc(orderParam, cb, cq, policyPath);
                        break;
                    case "RequestDate":
                        policyPath = policyRoot.get("requestDate");
                        getAscOrDesc(orderParam, cb, cq, policyPath);
                        break;
                    case "RequestNumber":
                        policyPath = policyRoot.get("requestNumber");
                        getAscOrDesc(orderParam, cb, cq, policyPath);
                        break;
                    case "Premium":
                        policyPath = policyRoot.get("simulationOffer").get("premium");
                        getAscOrDesc(orderParam, cb, cq, policyPath);
                        break;
                }
            }
            filteredPolicies = session.createQuery(cq).getResultList();
            return filteredPolicies;
        }
    }

    @Override
    public void update(PolicyRequest policyRequest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(policyRequest);
            session.getTransaction().commit();
        }
    }

    private void getAscOrDesc(String orderParam, CriteriaBuilder cb, CriteriaQuery<PolicyRequest> cq, Path<PolicyRequest> policyPath) {
        if (orderParam.equals("Descending")) {
            cq.orderBy(cb.desc(policyPath));
        } else {
            cq.orderBy(cb.asc(policyPath));
        }
    }

}
