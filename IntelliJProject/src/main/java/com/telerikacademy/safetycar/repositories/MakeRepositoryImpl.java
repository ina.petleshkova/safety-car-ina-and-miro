package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.exceptions.EntityNotFoundException;
import com.telerikacademy.safetycar.models.Make;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MakeRepositoryImpl implements MakeRepository {
    SessionFactory sessionFactory;

    @Autowired
    public MakeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Make> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Make> query = session.createQuery("from Make order by id", Make.class);
            return query.list();
        }
    }

    @Override
    public Make getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Make make = session.get(Make.class, id);
            if (make == null) {
                throw new EntityNotFoundException(
                        String.format("Make with ID %d not found!", id));
            }
            return make;
        }
    }
}
