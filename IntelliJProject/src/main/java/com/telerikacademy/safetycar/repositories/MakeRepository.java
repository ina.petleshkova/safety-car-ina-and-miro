package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.Make;

import java.util.List;

public interface MakeRepository {

    List<Make> getAll();

    Make getById(int id);

}
