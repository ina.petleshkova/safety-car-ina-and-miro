package com.telerikacademy.safetycar.repositories;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.VerificationToken;

public interface VerificationTokenRepository {

    VerificationToken findByToken(String token);

    VerificationToken findByUser(User user);

    void save(VerificationToken verificationToken);

}
