package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.UserPasswordChangeDTO;
import com.telerikacademy.safetycar.models.dtos.UserRegistrationDTO;

public interface UserService {

    void update(User userToUpdate, User loggedUser);

    void delete(User userToDelete, User loggedUser);

    User getByEmail(String email);

    org.springframework.security.core.userdetails.User changeSecurityUserPass(UserPasswordChangeDTO dto, org.springframework.security.core.userdetails.UserDetails loggedUser);
}
