package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Make;
import com.telerikacademy.safetycar.repositories.MakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MakeServiceImpl implements MakeService {
    private final MakeRepository makeRepository;

    @Autowired
    public MakeServiceImpl(MakeRepository makeRepository) {
        this.makeRepository = makeRepository;
    }

    @Override
    public List<Make> getAll() {
        return makeRepository.getAll();
    }

    @Override
    public Make getById(int id) {
        return makeRepository.getById(id);
    }

}
