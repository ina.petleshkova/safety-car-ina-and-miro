package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.models.dtos.SimulationOfferDTO;

import java.util.List;

public interface SimulationOfferService {

    List<SimulationOffer> getAll();

    SimulationOffer getById(int id);

    void create(SimulationOffer simulationOffer);

    void update(SimulationOffer simulationOffer);

    double calculatePremium(SimulationOffer simulationOffer);
}
