package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.models.dtos.ContactDTO;

import javax.mail.MessagingException;

public interface EmailService {

    void sendRegistrationMail(User user);

    void sendStatusChangeEmail(PolicyRequest policyRequest, String email);

    void contactEmail(ContactDTO dto);

}
