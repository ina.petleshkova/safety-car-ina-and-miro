package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.exceptions.EmailNotSentException;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.VerificationToken;
import com.telerikacademy.safetycar.models.dtos.ContactDTO;
import com.telerikacademy.safetycar.repositories.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static com.telerikacademy.safetycar.utils.Constants.SAFETY_CAR_EMAIL_ADDRESS;

@Service
@PropertySource("classpath:application.properties")
public class EmailServiceImpl implements EmailService {

    private final VerificationTokenRepository verificationTokenRepository;
    private final SpringTemplateEngine templateEngine;
    private final JavaMailSender javaMailSender;

    @Autowired
    public EmailServiceImpl(VerificationTokenRepository verificationTokenRepository,
                            SpringTemplateEngine templateEngine,
                            JavaMailSender javaMailSender) {
        this.verificationTokenRepository = verificationTokenRepository;
        this.templateEngine = templateEngine;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendRegistrationMail(User user) {
        try {
            VerificationToken verificationToken = verificationTokenRepository.findByUser(user);
            if (verificationToken != null) {
                String token = verificationToken.getToken();
                Context context = new Context();
                context.setVariable("title", "Verify your email address");
                context.setVariable("link", "http://localhost:8080/activation?token=" + token);
                String body = templateEngine.process("verification", context);
                MimeMessage message = javaMailSender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(message, true);
                helper.setTo(user.getUsername());
                helper.setSubject("Email address verification");
                helper.setText(body, true);
                javaMailSender.send(message);
            }
        } catch (MessagingException e) {
            throw new EmailNotSentException("\"Verify your email address\"", user.getUsername());
        }
    }

    @Override
    public void sendStatusChangeEmail(PolicyRequest policyRequest, String email) {
        try {
            Context context = new Context();
            context.setVariable("title", "The status of your policy request has been changed!");
            context.setVariable("firstName", policyRequest.getFirstName());
            context.setVariable("lastName", policyRequest.getLastName());
            context.setVariable("status", policyRequest.getStatus().getStatusName());
            context.setVariable("requestNumber", policyRequest.getRequestNumber());
            String body = templateEngine.process("status-change-email", context);
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(email);
            helper.setSubject("Policy request status changed");
            helper.setText(body, true);
            javaMailSender.send(message);
            context.removeVariable("firstName");
            context.removeVariable("lastName");

            if (!email.equals(policyRequest.getOwner().getEmail())) {
                context.setVariable("firstName", policyRequest.getOwner().getFirstName());
                context.setVariable("lastName", policyRequest.getOwner().getLastName());
                context.setVariable("holderFirstName", policyRequest.getFirstName());
                context.setVariable("holderLastName", policyRequest.getLastName());
                String bodyOwner = templateEngine.process("status-change-mail-owner", context);
                MimeMessage messageOwner = javaMailSender.createMimeMessage();
                MimeMessageHelper helperOwner = new MimeMessageHelper(messageOwner, true);
                helperOwner.setTo(policyRequest.getOwner().getEmail());
                helperOwner.setSubject("Policy request status changed");
                helperOwner.setText(bodyOwner, true);
                javaMailSender.send(messageOwner);
            }

        } catch (MessagingException e) {
            throw new EmailNotSentException("\"Policy request status changed\"", email);
        }
    }

    @Override
    public void contactEmail(ContactDTO dto) {
        try {
            Context context = new Context();
            context.setVariable("title", dto.getName());
            context.setVariable("message", dto.getMessage());
            context.setVariable("email", dto.getEmail());
            String body = templateEngine.process("contact-email", context);
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(SAFETY_CAR_EMAIL_ADDRESS);
            helper.setSubject(dto.getSubject());
            helper.setText(body, true);
            javaMailSender.send(message);

        } catch (MessagingException e) {
            throw new EmailNotSentException("\"Email not delivered\"", SAFETY_CAR_EMAIL_ADDRESS);
        }
    }

}
