package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Status;
import com.telerikacademy.safetycar.repositories.StatusRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {
    private final StatusRepository repository;

    public StatusServiceImpl(StatusRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Status> getAll() {
        return repository.getAll();
    }

    @Override
    public Status getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Status getByStatusName(String statusName) {
        return repository.getByStatus(statusName);
    }
}
