package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.Model;
import com.telerikacademy.safetycar.repositories.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {
    private final ModelRepository modelRepository;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Override
    public List<Model> getAll() {
        return modelRepository.getAll();
    }

    @Override
    public Model getById(int id) {
        return modelRepository.getById(id);
    }

    @Override
    public List<Model> getByMakeId(int makeId) {
        return modelRepository.getByMakeId(makeId);
    }
}
