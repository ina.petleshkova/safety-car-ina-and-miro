package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.PolicyRequest;
import com.telerikacademy.safetycar.models.SimulationOffer;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;

import java.util.List;

public interface PolicyRequestService {

    List<PolicyRequest> getAll();

    PolicyRequest getById(int id);

    List<PolicyRequest> getByCreator(int userId);

    void create(PolicyRequest policyRequest);

    List<PolicyRequest> filter(Integer status,
                               String creatorEmail,
                               String holderEmail,
                               Integer requestNumber,
                               String sortParam,
                               String orderParam);

    void update(PolicyRequest policyRequest);

    void approveOrDeny(PolicyRequest policyRequest, User loggedUser, String status);

    void cancel(PolicyRequest policyRequest, User loggedUser);

}
