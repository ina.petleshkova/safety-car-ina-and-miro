package com.telerikacademy.safetycar.services;



import com.telerikacademy.safetycar.models.Model;

import java.util.List;

public interface ModelService {

    List<Model> getAll();

    Model getById(int id);

    List<Model> getByMakeId(int makeId);

}
