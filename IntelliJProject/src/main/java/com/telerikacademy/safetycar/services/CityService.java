package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.City;

import java.util.List;

public interface CityService {

    List<City> getAll();

    City getById(int id);

}
