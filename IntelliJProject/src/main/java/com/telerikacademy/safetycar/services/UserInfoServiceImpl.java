package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.DuplicateEntityException;
import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;
import com.telerikacademy.safetycar.repositories.UserInfoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.telerikacademy.safetycar.utils.Constants.EMAIL_TAKEN_ERROR_MESSAGE;
import static com.telerikacademy.safetycar.utils.Constants.NOT_AUTHORIZED_TO_MODIFY_USERS_ERROR_MSG;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    private final UserInfoRepository userInfoRepository;

    public UserInfoServiceImpl(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

    @Override
    public void create(UserInfo userInfo) {
        checkIfEmailTaken(userInfo);
        userInfo.setDeleted(false);
        userInfoRepository.create(userInfo);
    }

    @Override
    public List<UserInfo> getAll() {
        return userInfoRepository.getAll();
    }

    @Override
    public UserInfo getById(int id) {
        return userInfoRepository.getById(id);
    }

    @Override
    public void update(UserInfo userInfo) {
        checkIfEmailTaken(userInfo);
        userInfoRepository.update(userInfo);
    }

    @Override
    public void modify(UserInfo userInfo, User loggedUser) {
        if (!loggedUser.isAdmin()) {
        throw new UnauthorizedOperationException(NOT_AUTHORIZED_TO_MODIFY_USERS_ERROR_MSG);
        }
            checkIfEmailTaken(userInfo);
            userInfoRepository.update(userInfo);
    }

    @Override
    public UserInfo getByEmail(String email) {
        return userInfoRepository.getByEmail(email);
    }

    private void checkIfEmailTaken(UserInfo userInfo) {
        if (userInfoRepository.existsByEmail(userInfo)) {
            throw new DuplicateEntityException(EMAIL_TAKEN_ERROR_MESSAGE);
        }
    }
}
