package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.VerificationToken;

import java.sql.Timestamp;

public interface VerificationTokenService {

    VerificationToken findByToken(String token);

    VerificationToken findByUser(User user);

    Timestamp calculateExpiry(int expiryTimeInMinutes);

    void save(User user, String token);


}
