package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.exceptions.UnauthorizedOperationException;
import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.dtos.UserPasswordChangeDTO;
import com.telerikacademy.safetycar.repositories.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.telerikacademy.safetycar.utils.Constants.*;

@Service
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void update(User userToUpdate, User loggedUser) {
        if (loggedUser.isAdmin() || loggedUser.getUsername().equals(userToUpdate.getUsername())) {
            userRepository.update(userToUpdate);
        } else {
            throw new UnauthorizedOperationException(NOT_ALLOWED_TO_EDIT_USERS_ERROR_MESSAGE);
        }
    }

    @Override
    public void delete(User userToDelete, User loggedUser) {
        if (loggedUser.isAdmin()) {
            userRepository.delete(userToDelete);
        } else {
            throw new UnauthorizedOperationException(NOT_ALLOWED_TO_DELETE_USERS_ERROR_MESSAGE);
        }
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }


    public org.springframework.security.core.userdetails.User changeSecurityUserPass(UserPasswordChangeDTO dto, org.springframework.security.core.userdetails.UserDetails loggedUser) {

        String encodedPassword = passwordEncoder.encode(dto.getNewPassword());

        org.springframework.security.core.userdetails.User updatedUser =
                new org.springframework.security.core.userdetails.User(
                        loggedUser.getUsername(), encodedPassword, true, true, true, true, loggedUser.getAuthorities());

        return updatedUser;
    }
}
