package com.telerikacademy.safetycar.services;

import com.telerikacademy.safetycar.models.User;
import com.telerikacademy.safetycar.models.UserInfo;

import java.util.List;

public interface UserInfoService {

    void create(UserInfo user);

    List<UserInfo> getAll();

    UserInfo getById(int id);

    void update(UserInfo userInfo);

    void modify(UserInfo userInfo, User loggedUser);

    UserInfo getByEmail(String email);

}
