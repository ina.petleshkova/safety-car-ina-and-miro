package com.telerikacademy.safetycar.models.dtos;

public class UpdatePolicyStatusDTO {

    private String status;

    public UpdatePolicyStatusDTO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
