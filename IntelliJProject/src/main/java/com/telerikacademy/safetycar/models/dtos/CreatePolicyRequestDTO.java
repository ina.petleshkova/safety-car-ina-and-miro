package com.telerikacademy.safetycar.models.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import static com.telerikacademy.safetycar.utils.Constants.*;


public class CreatePolicyRequestDTO {

    @JsonFormat(pattern= DATE_FORMAT)
    private String startDate;

    @Size(min = MIN_POLICY_HOLDER_FIRST_NAME_LENGTH,
            max = MAX_POLICY_HOLDER_FIRST_NAME_LENGTH,
            message = POLICY_HOLDER_FIRST_NAME_ERROR_MESSAGE)
    @Pattern(regexp = NAME_FORMAT, message = NAME_FORMAT_ERROR_MSG)
    private String firstName;

    @Size(min = MIN_POLICY_HOLDER_LAST_NAME_LENGTH,
            max = MAX_POLICY_HOLDER_LAST_NAME_LENGTH,
            message = POLICY_HOLDER_LAST_NAME_ERROR_MESSAGE)
    @Pattern(regexp = NAME_FORMAT, message = NAME_FORMAT_ERROR_MSG)
    private String lastName;

    @Pattern(regexp = PHONE_FORMAT, message = PHONE_FORMAT_ERROR_MSG)
    @Size(max = POLICY_HOLDER_PHONE_MAX_LENGTH)
    private String phone;

    @Email(regexp = EMAIL_FORMAT, message = EMAIL_FORMAT_ERROR_MESSAGE)
    private String email;

    @Positive(message = CITY_ERROR)
    private int cityId;

    @Size(min = MIN_POLICY_HOLDER_POSTAL_ADDRESS_LENGTH,
            max = MAX_POLICY_HOLDER_POSTAL_ADDRESS_LENGTH,
            message = POLICY_HOLDER_POSTAL_ADDRESS_LENGTH_ERROR_MESSAGE)
    private String postalAddress;

    private MultipartFile vehicleRegistrationImage;

    public CreatePolicyRequestDTO() {
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public MultipartFile getVehicleRegistrationImage() {
        return vehicleRegistrationImage;
    }

    public void setVehicleRegistrationImage(MultipartFile vehicleRegistrationImage) {
        this.vehicleRegistrationImage = vehicleRegistrationImage;
    }
}
