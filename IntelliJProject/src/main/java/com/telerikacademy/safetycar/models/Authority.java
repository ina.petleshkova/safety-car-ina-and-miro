package com.telerikacademy.safetycar.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import static com.telerikacademy.safetycar.utils.Constants.*;

@Entity
@Table(name = "authorities")
@IdClass(value = AuthorityId.class)
public class Authority {

    @Id
    @Column(name = "username")
    @Email(regexp = "^(.+)@(.+)$")
    private String username;

    @Id
    @Column(name = "authority")
    private String authority;

    public Authority() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
