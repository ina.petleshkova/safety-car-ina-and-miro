package com.telerikacademy.safetycar.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Entity
@Table(name = "policy_requests")
//@Loader(namedQuery = "getById")
//@NamedQuery(name = "getById", query =
//        "SELECT p " +
//                "FROM PolicyRequest p " +
//                "WHERE " +
//                "    p.id = ?1 AND " +
//                "    p.deleted = false")
//@SQLDelete(sql = "UPDATE policy_requests SET deleted = '1' WHERE id = ?", check = ResultCheckStyle.COUNT)
//@Where(clause = "deleted = 0")
public class PolicyRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "end_date")
    private String endDate;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    @Column(name = "postal_address")
    private String postalAddress;

    @Column(name = "vehicle_reg_certificate_image")
    private byte[] vehicleRegistrationImage;

    @Column(name = "request_date")
    private String requestDate;

    @OneToOne
    @JoinColumn(name = "simulation_offer_id")
    private SimulationOffer simulationOffer;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private UserInfo owner;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "request_number")
    private int requestNumber;

    public PolicyRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getVehicleRegistrationImage() {
        return vehicleRegistrationImage;
    }

    public void setVehicleRegistrationImage(byte[] vehicleRegistrationImage) {
        this.vehicleRegistrationImage = vehicleRegistrationImage;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public UserInfo getOwner() {
        return owner;
    }

    public void setOwner(UserInfo owner) {
        this.owner = owner;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public SimulationOffer getSimulationOffer() {
        return simulationOffer;
    }

    public void setSimulationOffer(SimulationOffer simulationOffer) {
        this.simulationOffer = simulationOffer;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public int getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(int requestNumber) {
        this.requestNumber = requestNumber;
    }
}