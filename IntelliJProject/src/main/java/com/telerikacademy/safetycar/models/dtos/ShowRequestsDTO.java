package com.telerikacademy.safetycar.models.dtos;

import org.springframework.web.multipart.MultipartFile;

public class ShowRequestsDTO {

    private String firstName;

    private String lastName;

    private String startDate;

    private String endDate;

    private String phone;

    private String email;

    private int city;

    private String address;

    private MultipartFile vehicleRegistrationImage;

    private String requestDate;

    private int simulationOffer;

    private int ownerId;

    private String status;

    private int requestNumber;

    public ShowRequestsDTO() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public MultipartFile getVehicleRegistrationImage() {
        return vehicleRegistrationImage;
    }

    public void setVehicleRegistrationImage(MultipartFile vehicleRegistrationImage) {
        this.vehicleRegistrationImage = vehicleRegistrationImage;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public int getSimulationOffer() {
        return simulationOffer;
    }

    public void setSimulationOffer(int simulationOffer) {
        this.simulationOffer = simulationOffer;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(int requestNumber) {
        this.requestNumber = requestNumber;
    }
}
