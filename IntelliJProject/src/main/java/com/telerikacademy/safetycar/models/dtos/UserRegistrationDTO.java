package com.telerikacademy.safetycar.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.telerikacademy.safetycar.utils.Constants.*;

public class UserRegistrationDTO {

    @Size(min = MIN_USER_FIRST_NAME_LENGTH,
            max = MAX_USER_FIRST_NAME_LENGTH,
            message = USER_FIRST_NAME_LENGTH_ERROR_MESSAGE)
    @Pattern(regexp = NAME_FORMAT, message = NAME_FORMAT_ERROR_MSG)
    private String firstName;

    @Size(min = MIN_USER_LAST_NAME_LENGTH,
            max = MAX_USER_LAST_NAME_LENGTH,
            message = USER_LAST_NAME_LENGTH_ERROR_MESSAGE)
    @Pattern(regexp = NAME_FORMAT, message = NAME_FORMAT_ERROR_MSG)
    private String lastName;

    @Email(regexp = EMAIL_FORMAT,
            message = EMAIL_FORMAT_ERROR_MESSAGE)
    private String email;

    @Pattern(regexp = PHONE_FORMAT, message = PHONE_FORMAT_ERROR_MSG)
    @Size(max = POLICY_HOLDER_PHONE_MAX_LENGTH)
    private String phone;

    @Pattern(regexp = PASSWORD_PATTERN,
            message = USER_PASSWORD_ERROR_MESSAGE)
    private String password;

    private String passwordConfirmation;

    public UserRegistrationDTO() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
