package com.telerikacademy.safetycar.models.dtos;

import javax.validation.constraints.Positive;

import static com.telerikacademy.safetycar.utils.Constants.ID_SHOULD_BE_POSITIVE;

public class UpdatePolicyStatusRestDTO {

    @Positive(message = ID_SHOULD_BE_POSITIVE)
    private int id;

    private String status;

    public UpdatePolicyStatusRestDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
