package com.telerikacademy.safetycar.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.Nullable;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Entity
@Table(name = "simulation_offers")
public class SimulationOffer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "cubic_capacity")
    private int cubicCapacity;

    @Column(name = "first_registration_date")
    private String firstRegistrationDate;

    @Column(name = "driver_age")
    private int driverAge;

    @Column(name = "prev_year_accidents")
    private boolean prevYearAccidents;

    @Column(name = "premium")
    private double premium;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private Model model;

    public SimulationOffer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public String getFirstRegistrationDate() {
        return firstRegistrationDate;
    }

    public void setFirstRegistrationDate(String firstRegistrationDate) {
        this.firstRegistrationDate = firstRegistrationDate;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public boolean getPrevYearAccidents() {
        return prevYearAccidents;
    }

    public void setPrevYearAccidents(boolean prevYearAccidents) {
        this.prevYearAccidents = prevYearAccidents;
    }

    public double getPremium() {
        return premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }
}
