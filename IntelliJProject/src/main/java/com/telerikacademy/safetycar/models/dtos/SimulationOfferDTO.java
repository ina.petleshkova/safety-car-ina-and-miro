package com.telerikacademy.safetycar.models.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.*;
import static com.telerikacademy.safetycar.utils.Constants.*;

public class SimulationOfferDTO {

    @NumberFormat
    @PositiveOrZero
    @Max(MAX_CAR_CUBIC_CAPACITY)
    private int cubicCapacity;

    @JsonFormat(pattern=DATE_FORMAT)
    private String firstRegistrationDate;

    @Min(MIN_DRIVER_AGE)
    private int driverAge;

    private boolean prevYearAccidents;

    @NotNull(message = CAR_MAKE_ERROR_MESSAGE)
    @PositiveOrZero(message = CAR_MAKE_ERROR_MESSAGE)
    private Integer makeId;

    @NotNull(message = CAR_MODEL_ERROR_MESSAGE)
    @PositiveOrZero(message = CAR_MAKE_ERROR_MESSAGE)
    private Integer modelId;

    @PositiveOrZero
    private double premium;

    public SimulationOfferDTO() {
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public String getFirstRegistrationDate() {
        return firstRegistrationDate;
    }

    public void setFirstRegistrationDate(String firstRegistrationDate) {
        this.firstRegistrationDate = firstRegistrationDate;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public boolean getPrevYearAccidents() {
        return prevYearAccidents;
    }

    public void setPrevYearAccidents(boolean prevYearAccidents) {
        this.prevYearAccidents = prevYearAccidents;
    }

    public Integer getMakeId() {
        return makeId;
    }

    public void setMakeId(Integer makeId) {
        this.makeId = makeId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public double getPremium() {
        return premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }
}
