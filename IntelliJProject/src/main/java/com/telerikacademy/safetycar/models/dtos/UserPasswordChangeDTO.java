package com.telerikacademy.safetycar.models.dtos;

import javax.validation.constraints.Pattern;

import static com.telerikacademy.safetycar.utils.Constants.PASSWORD_PATTERN;
import static com.telerikacademy.safetycar.utils.Constants.USER_PASSWORD_ERROR_MESSAGE;

public class UserPasswordChangeDTO {

    private String currentPassword;

    @Pattern(regexp = PASSWORD_PATTERN,
            message = USER_PASSWORD_ERROR_MESSAGE)
    private String newPassword;

    private String passwordConfirmation;

    public UserPasswordChangeDTO() {
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
