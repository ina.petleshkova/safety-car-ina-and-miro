# Safety Car Web Application
Created by Ina Petleshkova and Miroslav Filipov
<br/><br/>
### SAFETY CAR is an insurance-oriented application for the end users. It allows simulation of amount and request insurance policies online. The application functionalities are related to three different type of users: public, private and agents.
The public part of application is visible without authentication. It provides the following functionalities: 
-   Simulation car insurance offer based on some input criteria; 
-   Creation of a new account and log-in functionality.  
  
The private part of the application is for registered users only and should be accessible after successful login. The main functionalities provided for this area are:
-   Send request for approval of offer (request policy);
-   List the history of the logged user’s requests, cancel pending requests.  
  
The agents of the system have permission to manage the request policies in the system. The main functionalities provided for this area are: 
-   List of requests created in the system;
-   Filter and the requests in the system;
-   Approve or reject requests.
  
    
## 1.   Business Glossary
### Term Description
- **Simulation Offer (Quote)** An estimate of expected value of insurance policy 
- **Insurance Policy** A legal contract between insurer and insured person 
- **Premium** The final amount of insurance policy to be paid by the client after taxes 
<br/><br/>
## 2.   Functionalities
In the following sections different functionalities will be described in more details. 
<br/>
### 2.1.    Create Account
A user can register into the web app by providing their names, phone, valid email address and password. The registration is complete via email confirmation link.
<br/>
### 2.2.    Simulate Offer
This functionality is accessible from public and private part. 
In the SIMULATE OFFER page the user can input the specific parameters of their car and visualize the amount of expected total premium. 
The simulation form should contain these business inputs: 
-   car (brand and model)
-   cubic capacity
-   first registration date
-   driver age
-   accidents in previous year (yes/no)  
  
After a click on the "Calculate premium" button, the expected premium is calculated and shown on the screen.  
When the total premium amount is calculated, the user has the possibility to request this offer for approval by clicking "PROCEED TO POLICY REQUEST". If the user has not been logged in, the system will guide them to log in. If the user is logged in, they can proceed to the page for policy request preparation.  
  
### 2.3.    Request policy
This functionality is accessible only from the private part of the application.  
In order to issue a final policy, the user goes to "REQUEST POLICY" and enters the addition needed data.  
One user can request a policy for themselves or for another person.  
The policy request includes the following fields: 
-   name of the policy holder; 
-   effective date of the policy; 
-   attachment of image of vehicle registration certificate; 
-   communication details: email, phone, city, postal address.  
  
All details from the simulated offer are visible in the page.  
Once the policy is requested it is stored in the system for further management. At that stage request becomes in state “pending” and should be treated by the agents of the system.  
  
### 2.4.    User’s Request History
This functionality is accessible only from the private part of the application. 
All policy requests for the logged user with their details are displayed. The history of all requests is shown (pending, approved, rejected and cancelled) and chronologically sorted.  
-   The user can cancel requests which are pending (not treated by agent)  
  
### 2.5.    Manage Requests
This functionality is accessible only from agent part of the application.  
Implement a page to display list of all pending requests in the system with their details. The agent should be able to accept or reject the request.  
Optional functionalities:  
-   Possibility to filter requests by different criteria (user, request date, etc.) 
-   The data of multicriteria range table to be configured and loaded from external source 
-   Send email to the user in case of approval/rejection of the request   
  
## 3. Technical Description
### 3.1. Database
The data of the application is stored in a relational database – MariaDB.  
  
![database](Screenshots/database.PNG)  
  
### 3.2. Backend
-   JDK version 11  
-   Layered system - controllers, services, repositories  
-   SpringMVC controllers and REST controllers 
-   Hibernate in the repository layer  
-   Spring Security used to handle user registration and user roles  
-   Mockito used for testing the service layer  
  
### 3.3. Frontend
-   Spring MVC Framework with Thymeleaf template engine for generating the UI
-   JAX for making asynchronous requests to the server 
-   Bootstrap used  
  
### 3.4 Documentation
- Swagger used for documentation  
  
    
## 4. UI Screenshots  

### Homepage  
  
![home](Screenshots/home.PNG)

### Register  
  
![register](/Screenshots/register.PNG)
  
    

### Simulate Offer  
  
![simulate-offer](/Screenshots/simulate-offer.PNG)  
  
    

### Request Policy  
  
![request-policy](/Screenshots/request-policy.PNG)  
  
    
      

### My requests (User)  
  
![request-policy](/Screenshots/my-policies.png)  
  
    
      

### Requests (Agent)  
  
![requests](/Screenshots/requests-agent.png)  
  
    
      

### About Us  
  
![about-us](/Screenshots/about-us.PNG)  
  
    
      

### Contact  
  
![contact](/Screenshots/contact.PNG)    
  
    
      

### Log in  
  
![login](/Screenshots/log-in.PNG)  
  
    
      






